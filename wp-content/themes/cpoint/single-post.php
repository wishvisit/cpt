
<?php
/**
* Template Name: single post template
 *
 */

get_header();
?>

<main id="main-container" class="site-content container" role="main">
    <div class="row">
        <h2 id="post-side-header" class="d-block d-md-none">Events & News</h2>
        <aside class="col-12 col-md-3 single-side-bar">
        <h2 id="post-side-header"  class="d-block d-xs-none d-sm-none d-md-block">Events & News</h2>
            <ul id="post-side-list">
               
            <?php 
                 $args = array(  
                    'post_status' => 'publish',
                    'orderby' => 'post_date', 
                    'order' => 'desc',
                );
            
                $loop = new WP_Query( $args ); 
                    
                while ( $loop->have_posts() ) : $loop->the_post();
                    //$featured_img = wp_get_attachment_image_src( $post->ID );?>
                      
                        <li class="aside-news-item">
                        <a href="<?php echo the_permalink();?>">
                                <?php echo the_title();?>
                                </a>
                        </li>
                        
                <?php endwhile;
            
                wp_reset_postdata(); 
            ?>
            </ul>   
        </aside>
        <article class="col-12 col-md-9 single-post-content">
            <div id="content-thumbnail">
                <?php the_post_thumbnail();?>
            </div>
            <div id="content-body">
                <?php the_content();?>
            </div>
        </article>
    </div>
</main><!-- #site-content -->
<?php
get_footer();