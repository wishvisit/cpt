
<?php
/**
* Template Name: News & Event
 */

get_header();
?>

<main id="main-container" class="site-content container" role="main">
    <div class="row">
        <div class="col-12 contoller">
            <h2 class="news-event-title">Event & News</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-12 news-list-container">
            <div class="row">
            <?php 
                 $args = array(  
                    'post_status' => 'publish',
                    'orderby' => 'post_date', 
                    'order' => 'desc',
                );
            
                $loop = new WP_Query( $args ); 
                    
                while ( $loop->have_posts() ) : $loop->the_post();
                    //$featured_img = wp_get_attachment_image_src( $post->ID );?>
                    <article class="col-6 col-md-3 news-item">
                        <a href="<?php the_permalink();?>" >
                            <div class="thumbnail"><?php the_post_thumbnail();?></div>
                            <div class="title"><?php echo the_title();?></div>
                        </a>
                    </article>
                <?php endwhile;
            
                wp_reset_postdata(); 
            ?>
            </div>
        </div>
    </div>
</main><!-- #site-content -->
<?php
get_footer();