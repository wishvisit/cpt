<?php
/**
 * The template for displaying the footer
 */
?>
<footer id="site-footer" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 footer-address">
                <address>
                    <h4>CPoint (Thailand) Corp.,Ltd</h4>
                    <p>24 Prime Building. 11 Fl. Room 11A. Sukhunvit 21 (Asokemontree).<br>
                    Sukhunvit Rd. Klong Toei Nua. Watthana, Bangkok 10110</p>
                </address>
                <div class="footer-contact">
                    <a href="tel:"><span id="contact-icon"></span>02-3536-333</a>
                    <a href="mailto:"><span id="mail-icon"></span>info@c-point.co.th</a>
                    <a href="fb"><span id="fb-icon"></span>Facebook</a>   
                </div>
                <div class="footer-copyright">
                <p>&copy;
                    <?php
                    echo date_i18n(
                        _x( 'Y', 'copyright date format', 'cpt' )
                    );
                    ?>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
                </p>
                </div>
            </div>
            <div class="col-12 col-md-6 footer-link">
                <div class="row">
                    <div class="col-12 col-md-6 footer-service text-center v-center">
                        <a href="https://www.jgbthai.com/" target="_blank">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/footer-jgb.png" alt="jgb" />
                            JGB
                        </a>
                        <a href="https://www.purioth.com/" target="_blank">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/footer-purio.png" alt="purio" />
                            Purio
                        </a>
                        <a href="https://www.namjai.cc/" target="_blank">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/footer-namjai.png" alt="namjai" />
                            Namjai
                        </a>
                        <a href="https://www.c-point.co.jp/" target="_blank">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/footer-cpoint.png" alt="namjai" />
                            Cpoint JP
                        </a>
                    </div>
                    <div class="col-12 col-md-6 footer-map">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.6460729216906!2d100.55988961527399!3d13.739865001270767!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29ee4bd0966bd%3A0x362f07e8534a3306!2sCPOINT%20(THAILAND)%20CROP.%2CLTD.!5e0!3m2!1sen!2sth!4v1590906267351!5m2!1sen!2sth" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- #site-footer -->
<?php wp_footer(); ?>
</body>
</html>
