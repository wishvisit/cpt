<?php
/**
 * homepage template file
 *
 */

get_header();
?>

<main id="main-container" class="site-content container" role="main">
    <div class="row">
        <div class="col-12">
        <div id="home-slide" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#home-slide" data-slide-to="0" class="active"></li>
            <li data-target="#home-slide" data-slide-to="1"></li>
            <li data-target="#home-slide" data-slide-to="2"></li>
            <li data-target="#home-slide" data-slide-to="3"></li>
            <li data-target="#home-slide" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="<?php echo get_template_directory_uri()?>/assets/images/BusinessConsult.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100"src="<?php echo get_template_directory_uri()?>/assets/images/BusinessMatching.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo get_template_directory_uri()?>/assets/images/Product Survey.jpg" alt="Third slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo get_template_directory_uri()?>/assets/images/Seminar.jpg" alt="Third slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo get_template_directory_uri()?>/assets/images/ShowOnstage.jpg" alt="Third slide"> 
            </div>
        </div>
        <a class="carousel-control-prev" href="#home-slide" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#home-slide" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        </div>
        </div>
    </div>
</main><!-- #site-content -->
<?php
get_footer();
