<?php
/**
* Template Name: Registration
 *
 */

get_header();
?>

<main id="main-container" class="site-content container" role="main">
    <div class="row">
        <div class="col-12 col-md-6 registration-form-container">
            <h2>Registration</h2>
            <?php echo do_shortcode('[contact-form-7 id="40" title="Contact form 1"]');?>
         <?php /*   <div class="form-group">
                <label for="exampleInputEmail1">Name <span class="red">*</span></label>
                [text* Name id:contact-name placeholder "Name"]
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Name <span class="red">*</span></label>
                [text* Companyname id:company-name placeholder "Company name"]
            </div>*/?>
        </div>
        <div class="col-12 col-md-6 d-none  d-md-block images-container">
            <img src="<?php echo get_template_directory_uri()?>/assets/images/registration.png" alt="registration" />
        </div>
    </div>
</main><!-- #site-content -->
<?php
get_footer();