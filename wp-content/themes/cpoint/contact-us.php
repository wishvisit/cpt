<?php
/**
* Template Name: Contact us
 *
 */

get_header();
?>

<main id="main-container" class="site-content container" role="main">
    <div class="row">
        <div class="col-12 col-md-6 contact-form-container v-center">
            <h2 class="contact-title">Contact us</h2>
            <?php echo do_shortcode('[contact-form-7 id="41" title="Contact Form"]');?>
         <?php /*   <div class="form-group">
                <label for="exampleInputEmail1">Name <span class="red">*</span></label>
                [text* Name id:contact-name placeholder "Name"]
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Name <span class="red">*</span></label>
                [text* Companyname id:company-name placeholder "Company name"]
            </div>*/?>
        </div>
        <div class="col-12 col-md-6 d-none  d-md-block contact-images-container">
            <img src="<?php echo get_template_directory_uri()?>/assets/images/contact-us.png" alt="contact-us" />
        </div>
    </div>
</main><!-- #site-content -->
<?php
get_footer();