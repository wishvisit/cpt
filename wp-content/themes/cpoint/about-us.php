<?php /* Template Name: About us */
get_header();
?>
<main id="aboutus-container" class="site-content container" role="main">
    <div class="row">
        <div class="col-12">
            <?php while (have_posts()) : the_post(); ?>
            <?php //echo the_content();
                ?>
            <div id="fullpage">
                <?php //echo the_content();
                    ?>
                <div class="section fp-auto-height-responsive" id="profile">
                    <div class="full-page-content">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <img class="d-block about-us-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/about-us.jpg"
                                        alt="First slide">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center about-us-text">
                                        <p>บริษัท CPOINT (THAILAND) CORP.,LTD.</p>
                                        <p>บริษัทสัญชาติญี่ปุ่น ผู้ให้บริการด้านคำแนะนำและส่งเสริมธุรกิจระหว่างประเทศไทย
                                            โดยมีสำนักงานใหญ่ในจังหวัดชิซึโอกะ
                                            และสาขาในต่างประเทศรวมทั้งบริษัทในเครืออีกมากมาย อาทิในประเทศ จีน เวียดนาม
                                            และ ฮ่องกง</p>
                                        <p>บริษัท CPOINT (THAILAND) ก่อตั้งขึ้นในประเทศไทยตั้งแต่ปี 2008
                                            โดยมุ่งให้บริการด้านธุรกิจครบวงจร ทั้งออนไลน์และออฟไลน์
                                            ตั้งแต่การให้คำแนะนำในการทำตลาดทั้งในไทยและญี่ปุ่น
                                            การสร้างและบริหารช่องทางการตลาด การทำวิจัยตลาด การออกแบบกราฟฟิคและสิ่งพิมพ์
                                            การจัดกิจกรรมอีเวนท์ การจัดทำสื่อโฆษณาและช่องทางโปรโมท
                                            ไปจนถึงการทำธุรกิจระหว่างประเทศ ด้วยประสบการณ์กว่า10ปี
                                            และทีมงานที่พร้อมช่วยเหลือรอบด้าน
                                            ให้คุณสามารถดำเนินธุรกิจได้อย่างราบรื่นและมีประสิทธิภาพ
                                            ไม่ว่าคุณจะอยู่ในประเทศไทย หรือ ญี่ปุ่น</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section fp-auto-height-responsive">
                    <div class="full-page-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <img class="d-none vision-img d-md-block"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/vision.jpg"
                                        alt="vision">
                                </div>
                                <div class="col-12 col-md-6">
                                    <img class="d-block vision-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/vision-text.jpg"
                                        alt="vision">
                                    <?php /*
                                        <div class="text-center vision">
                                            <p>พวกเรา CPOINT มีพันธกิจและความมุ่งมั่นในการเป็นตัวกลางเชื่อมต่อ ส่งเสริม สนับสนุน
                                                การทำธุรกิจรวมทั้งการขยายตลาดระหว่างธุรกิจไทยและธุรกิจญี่ปุ่น</p>

                                            <p>ด้วยประสบการณ์กว่า10ปี และทีมงานที่พร้อมช่วยเหลือทุกด้าน
                                                ให้คุณสามารถดำเนินธุรกิจได้อย่างราบรื่นและมีประสิทธิภาพ
                                                ไม่ว่าคุณจะอยู่ในประเทศไทย หรือ ญี่ปุ่น</p>
                                        </div>*/?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section fp-auto-height-responsive">
                    <div class="full-page-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/Bankoku Magazine.jpg"
                                        alt="Bankoku Magazine">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/funai soken.jpg"
                                        alt="funai soken">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/hakuhodo.jpg"
                                        alt="hakuhodo">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/dentsu.png"
                                        alt="dentsu">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/odakyu.jpg"
                                        alt="odakyu">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/RingerHut.jpg"
                                        alt="RingerHut">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/jalux.jpeg"
                                        alt="jalux">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/JR East.jpg"
                                        alt="JR East">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/asahi.jpg"
                                        alt="asahi">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/JCB.jpg"
                                        alt="JCB">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/Centrair.jpg"
                                        alt="Centrair">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/Hoshino Resort.jpg"
                                        alt="Hoshino Resort">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/SecondStage.png"
                                        alt="SecondStage">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/krungsri.png"
                                        alt="krungsri">
                                </div>
                                <div class="col-3 col-md-2 v-center">
                                    <img class="client-img"
                                        src="<?php echo get_template_directory_uri() ?>/assets/images/Tatsune.png"
                                        alt="Tatsune">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php /*
                <div class="section fp-auto-height-responsive">
                    <div class="full-page-content">
                        <img class="alignnone size-full wp-image-30" src="http://localhost/cpt/wp-content/uploads/2020/05/dummy-content3.jpg" alt="" width="1588" height="896">
                    </div>
                </div>*/ ?>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
    <style>
        section#desktop-top-menu {
            position: fixed;
            top: 0;
            width: 100%;
            background: white;
        }

        div.full-page-content {
            /* padding: 150px 0; */
        }

        footer#site-footer {
            position: fixed;
            bottom: 0;
            width: 100%;
            height: 160px;
        }
    </style>
</main><!-- #site-content -->
<?php get_footer() ?>