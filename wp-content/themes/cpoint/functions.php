<?php
/**
 * cpt functions and definitions
 */

add_filter( 'intermediate_image_sizes_advanced', 'prefix_remove_default_images' );

// Remove default image sizes here. 
function prefix_remove_default_images( $sizes ) {
unset( $sizes['small']); // 150px
unset( $sizes['medium']); // 300px
unset( $sizes['large']); // 1024px
unset( $sizes['medium_large']); // 768px
return $sizes;
}

function theme_support() {
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// Custom logo.
	$logo_width  = 120;
	$logo_height = 90;

	// If the retina setting is active, double the recommended width and height.
	if ( get_theme_mod( 'retina_logo', false ) ) {
		$logo_width  = floor( $logo_width * 2 );
		$logo_height = floor( $logo_height * 2 );
	}

	add_theme_support(
		'custom-logo',
		array(
			'height'      => $logo_height,
			'width'       => $logo_width,
			'flex-height' => true,
			'flex-width'  => true,
		)
	);
	add_theme_support( 'title-tag' );
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'style',
		)
	);
	//load_theme_cpt( 'cpt' );
	add_theme_support( 'align-wide' );
	add_theme_support( 'responsive-embeds' );
	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}

add_action( 'after_setup_theme', 'theme_support' );

function register_styles() {

	$theme_version = wp_get_theme()->get( 'Version' );

	wp_enqueue_style( 'cpoint-style', get_stylesheet_uri(), array(), $theme_version );
	wp_style_add_data( 'cpoint-style', 'rtl', 'replace' );
	if( is_page_template('about-us.php')){
		wp_enqueue_style( 'fullpage-style', get_template_directory_uri() . '/assets/css/fullpage.min.css', null, $theme_version );
	}
	// Additional CSS.
	wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/assets/css/bootstrap.css', null, $theme_version );

}

add_action( 'wp_enqueue_scripts', 'register_styles' );

function register_scripts() {

	$theme_version = wp_get_theme()->get( 'Version' );

	wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/assets/js/jquery-3.5.1.min.js', array(), $theme_version, true );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array('jquery-js'), $theme_version, true );
	if( is_page_template('about-us.php')){
		wp_enqueue_script( 'fullpage-js', get_template_directory_uri() . '/assets/js/fullpage2.9.7.min.js', array('jquery-js'), $theme_version, true );
		wp_enqueue_script( 'fullpage-js-custom', get_template_directory_uri() . '/assets/js/custom-fullpage.js', array('jquery-js',"fullpage-js"), $theme_version, true );
    }
	if( is_page_template('our-service.php')){
		wp_enqueue_script( 'ourservice-custom', get_template_directory_uri() . '/assets/js/custom-ourservice.js', array('jquery-js'), $theme_version, true );
		wp_localize_script('ourservice-custom', 'ourservice', array( 'ajax_url' => admin_url('admin-ajax.php'),'ajax_nonce' => wp_create_nonce('ourservice')));
	}
	if( is_page_template('portfolio.php')){
		wp_enqueue_script( 'portfolio-custom', get_template_directory_uri() . '/assets/js/custom-portfolio.js', array('jquery-js'), $theme_version, true );
		wp_localize_script('portfolio-custom', 'portfolio', array( 'ajax_url' => admin_url('admin-ajax.php'),'ajax_nonce' => wp_create_nonce('ourservice')));
	}
	if( is_page_template('registration.php')){
		wp_enqueue_script( 'registration-custom', get_template_directory_uri() . '/assets/js/custom-registration.js', array('jquery-js'), $theme_version, true );		
    }
}

add_action( 'wp_enqueue_scripts', 'register_scripts' );

function register_menus() {

	$locations = array(
		'primary'  => __( 'Desktop Menu', 'cpt' ),
		'mobile'   => __( 'Mobile Menu', 'cpt' ),
		'footer'   => __( 'Footer Menu', 'cpt' ),
	);

	register_nav_menus( $locations );
}

add_action( 'init', 'register_menus' );

function cpt_post_type() {
    $service_labels = array(
        'name'                  => _x( 'Our services', 'Post type general name', 'cpt' ),
        'singular_name'         => _x( 'service', 'Post type singular name', 'cpt' ),
        'menu_name'             => _x( 'Our services', 'Admin Menu text', 'cpt' ),
        'name_admin_bar'        => _x( 'service', 'Add New on Toolbar', 'cpt' ),
        'add_new'               => __( 'Add New', 'cpt' ),
        'add_new_item'          => __( 'Add New service', 'cpt' ),       
    );
 
    $service_args = array(
        'labels'             => $service_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'service' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
		'hierarchical'       => false,
		'menu_icon'			 => 'dashicons-nametag',
		'menu_position'      => 4,
		'show_in_rest' 		 => true,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
	);
	$portfolio_labels = array(
        'name'                  => _x( 'Our portfolio', 'Post type general name', 'cpt' ),
        'singular_name'         => _x( 'portfolio', 'Post type singular name', 'cpt' ),
        'menu_name'             => _x( 'Our portfolios', 'Admin Menu text', 'cpt' ),
        'name_admin_bar'        => _x( 'portfolio', 'Add New on Toolbar', 'cpt' ),
        'add_new'               => __( 'Add New', 'cpt' ),
        'add_new_item'          => __( 'Add New portfolio', 'cpt' ),       
    );
 
    $portfolio_args = array(
        'labels'             => $portfolio_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'portfolio' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
		'hierarchical'       => true,
		'menu_icon'			 => 'dashicons-welcome-widgets-menus',
		'menu_position'      => 4,
		'show_in_rest' 		 => true,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt','page-attributes'),
	);
	
	//portfolio taxonomy
	$port_labels = array(
		'name' => _x( 'Portfolio Type', 'taxonomy general name' ),
		'singular_name' => _x( 'Portfolio Type', 'taxonomy singular name' ),
		'search_items' =>  __( 'Search Portfolio Type' ),
		'all_items' => __( 'All Portfolio Types' ),
		'parent_item' => __( 'Parent Portfolio Type' ),
		'parent_item_colon' => __( 'Parent Type:' ),
		'edit_item' => __( 'Edit Portfolio Type' ), 
		'update_item' => __( 'Update Portfolio Type' ),
		'add_new_item' => __( 'Add New Portfolio Type' ),
		'new_item_name' => __( 'New Portfolio Type' ),
		'menu_name' => __( 'Portfolio Types' ),
	);    
 
	register_taxonomy('portfolio-type',array('portfolio'), array(
		'hierarchical' => true,
		'labels' => $port_labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'portfolio-type' ),
	));
 
	register_post_type( 'service', $service_args );
	register_post_type( 'portfolio', $portfolio_args );
}
add_action( 'init', 'cpt_post_type' );

add_action( 'wp_ajax_nopriv_get_service_content', 'get_service_content');
add_action( 'wp_ajax_get_service_content', 'get_service_content');
function get_service_content(){
	//check_ajax_referer('ourservice', 'security');
    $html = '';
	$post_id		= $_POST['data'];
	$content = "<h2 class='content-title'>".get_the_title($post_id)."</h2>";
	$content .= apply_filters('the_content', get_post_field('post_content', $post_id));
	$content .= get_the_post_thumbnail($post_id);

	print_r(json_encode($content));
	exit();
}
