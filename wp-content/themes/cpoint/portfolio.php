<?php

/**
 * Template Name: Portfolio
 *
 */

get_header();
?>

<main id="main-container" class="site-content container port-page" role="main">
    <div class="row">
        <div class="col-12 col-md-3 portfolio-list">
            <h2 id="side-portfolio-header">portfolio</h2>
            <ul id="portfolio-list">
                <li class="port-item selected" data-post-id="135" >Online</li>
                <ul class="subitem1">
                    <li class="port-item" data-post-id="134" >Webiste</li>
                    <ul id="portfolio-online">
                        <?php $args = array(
                            'post_type' => 'portfolio',
                            'post_parent__in' => array(134),
                            'post_status' => 'publish',
                            'orderby'   => 'parent',
                            'order' => 'asc',
                        );
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();
                            //$featured_img = wp_get_attachment_image_src( $post->ID );
                        ?>
                            <li class="port-item" data-post-id="<?php echo $post->ID; ?>" data-service-name="<?php echo the_title(); ?>"><?php echo the_title(); ?></li>
                        <?php endwhile;

                        wp_reset_postdata();
                        ?>
                    </ul>
                    <li class="port-item" data-post-id="138" >Social Network</li>
                    <ul id="portfolio-social">
                        <?php $args = array(
                            'post_type' => 'portfolio',
                            'post_parent__in' => array(138),
                            'post_status' => 'publish',
                            'orderby'   => 'parent',
                            'order' => 'asc',
                        );
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();
                            //$featured_img = wp_get_attachment_image_src( $post->ID );
                        ?>
                            <li class="port-item" data-post-id="<?php echo $post->ID; ?>" data-service-name="<?php echo the_title(); ?>"><?php echo the_title(); ?></li>
                        <?php endwhile;
                        wp_reset_postdata();
                        ?>
                    </ul>
                </ul>
                <li class="port-item" data-post-id="144" >Offline</li>
                <ul id="portfolio-offline">
                        <?php $args = array(
                            'post_type' => 'portfolio',
                            'post_parent__in' => array(144),
                            'post_status' => 'publish',
                            'orderby'   => 'parent',
                            'order' => 'asc',
                        );
                        $loop = new WP_Query($args);
                        while ($loop->have_posts()) : $loop->the_post();
                            //$featured_img = wp_get_attachment_image_src( $post->ID );
                        ?>
                            <li class="port-item" data-post-id="<?php echo $post->ID; ?>" data-service-name="<?php echo the_title(); ?>"><?php echo the_title(); ?></li>
                        <?php endwhile;
                        wp_reset_postdata();
                        ?>
                    </ul>
            </ul>
        </div>
        <div class="col-12 col-md service-content">
            <div id="content-output">
                <img src="<?php echo get_template_directory_uri() ?>/assets/images/dummy-slide.jpg" alt="slide" />
            </div>
        </div>
    </div>
</main><!-- #site-content -->
<?php
get_footer();
