<section id="desktop-top-menu">
    <div class="container">
        <?php if( is_page_template('our-service.php')){
        get_template_part( 'template-parts/ourservice-top-menu' );
        }?>
        <div class="row">
            <div class="col-2">
                <span id="site-logo">
                    <a href="<?php echo get_home_url();?>"><img src="<?php echo get_template_directory_uri()?>/assets/images/cpoint-logo.png" alt="cpoint thailand logo"/>
                    </a>
                </span>
            </div>
            <div class="col-10 v-center">
            <nav class="navbar navbar-expand-lg navbar-light main-navbar">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="main-nav">
                <?php 
                    if ( has_nav_menu( 'primary' ) ) {

                        wp_nav_menu(
                            array(
                                'container_class' => "navbar-nav",
                                'items_wrap' => '%3$s',
                                'theme_location' => 'primary',
                            )
                        );

                    } 
                    ?>
                </div>
            </nav>
            </div>
        </div>
    </div>

</section>

