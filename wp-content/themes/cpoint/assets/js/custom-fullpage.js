$(document).ready(function() {
    console.log("fullpage ready");
    $('#fullpage').fullpage({
        //options here
        anchors: ["profile", "vision", "client"],
        autoScrolling: true,
        scrollHorizontally: true,
        fixedElements: '#desktop-top-menu, #site-footer',
    });
});