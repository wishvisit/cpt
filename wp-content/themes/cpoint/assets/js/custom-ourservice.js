$(document).ready(function() {
    console.log("ourservice ready");
    $('#how-to-modal').modal({
        keyboard: false,
        backdrop: 'static'
    })
    $('#how-to-slide').carousel({
        interval: 2000,
        wrap: false
    })
    get_service_content(85);
    check_interested();
    count_service();
    $('.service-item').click(function() {
        let service_name = $(this).attr("data-service-name");
        let service_id = $(this).attr("data-post-id");
        get_service_content(service_id);
        update_interest_service(service_id, service_name);
        check_interested();
    });
    $('#interest-service').click(function() {
        let service_id = $(this).attr("data-service-id");
        let service_name = $(this).attr("data-service-name");
        let selected = $(this).hasClass('interested');
        console.log(service_id, service_name, selected);
        set_interest_service(service_id, service_name, selected);
        check_interested();
    });
    $("#our-service-top-menu, #our-service-moboile").click(function() {
        $(".service-list").addClass("hidden");
        $(".selected-service").removeClass("hidden");
    });
    $(".interest-more").click(function() {
        $(".service-list").removeClass("hidden");
        $(".selected-service").addClass("hidden");
    });
});

function get_service_content(post_id) {
    $.ajax({
        type: 'post',
        url: ourservice.ajax_url,
        data: {
            action: 'get_service_content',
            data: post_id,
            security: ourservice.ajax_nonce
        },
        success: function(response) {
            response = JSON.parse(response);
            $('#content-output').html(response);
        }
    });
}

function set_interest_service(service_id, service_name, selected) {
    var service_list = get_interested_service();
    if (!service_list) {
        var service_list = {};
    }
    if (selected == false) {
        service_list[service_id] = service_name;
        service_list = JSON.stringify(service_list)
        localStorage.setItem("service", service_list);
    } else {
        delete service_list[service_id];
        service_list = JSON.stringify(service_list)
        localStorage.setItem("service", service_list);
    }
    count_service();

}

function update_interest_service(service_id, service_name) {
    $("#interest-service").attr("data-service-id", service_id).attr("data-service-name", service_name);
}

function get_interested_service() {
    let service_list = localStorage.getItem("service");
    if (service_list) {
        service_list = JSON.parse(service_list);
    }
    return service_list;
}

function check_interested() {
    let service_list = get_interested_service();
    if (service_list) {
        let select_service = $('#interest-service');
        let service_id = parseInt(select_service.attr("data-service-id"));
        let test = service_id in service_list;
        if (test) {
            select_service.addClass('interested');
        } else {
            select_service.removeClass('interested');
        }
    }
}

function count_service() {
    let service_list = get_interested_service();
    let count = 0;
    if (service_list) {
        count = Object.keys(service_list).length
    }
    $(".service-count").html(count);
    show_selected_service_list();
}

function show_selected_service_list() {
    let service_list = get_interested_service();
    let html = "";
    if (service_list) {
        for (var key in service_list) {
            if (service_list.hasOwnProperty(key)) {
                html += "<li class='selected-service-item'>" + service_list[key] + "</li>";
            }
        }
    } else {
        html = "no selected service";
    }
    $(".selected-list-output").html(html);
}