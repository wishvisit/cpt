$(document).ready(function() {
    get_service_content(135);
    $('.port-item').click(function() {
        $('.port-item').removeClass('selected');
        $(this).addClass('selected');
        let service_id = $(this).attr("data-post-id");
        get_service_content(service_id);
    });
});

function get_service_content(post_id) {
    $.ajax({
        type: 'post',
        url: portfolio.ajax_url,
        data: {
            action: 'get_service_content',
            data: post_id,
            security: portfolio.ajax_nonce
        },
        success: function(response) {
            response = JSON.parse(response);
            $('#content-output').html(response);
        }
    });
}