$(document).ready(function() {
    console.log('registration script ready');
    setTimeout(() => { update_selected_service(); }, 2000);

});

function get_interested_service() {
    let service_list = localStorage.getItem("service");
    if (service_list) {
        service_list = JSON.parse(service_list);
    }
    return service_list;
}

function update_selected_service() {
    let service_list = get_interested_service();
    let service_name = [];
    if (service_list) {
        for (var key in service_list) {
            if (service_list.hasOwnProperty(key)) {
                service_name.push(service_list[key]);
            }
        }
    }
    $('#selected-service').val(service_name);
    $('#selected-service').trigger('change');
    console.log(service_name);
}